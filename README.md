# Kubernetes



1. opening Docker Desktop app.
2. From the Docker Dashboard, select the Settings.
3. Select Kubernetes from the left sidebar.
4. Next to Enable Kubernetes, select the checkbox.
5. Select Apply & Restart to save the settings and then click Install to confirm. This instantiates images required to run the Kubernetes server as containers, and installs the /usr/local/bin/kubectl command on your machine.

## Use the kubectl command

ensure you change the context so that kubectl is pointing to docker-desktop:

```
kubectl config get-contexts
 kubectl config use-context docker-desktop

```

```

kubectl apply -f namespaces.yaml
```

check


```
kubectl get namespaces

```
